### Configuration for the folders with C++ files.

################################################################################
#           Folder that contains the class ClassName.                          #
################################################################################

### Add header file for the class ClassName.
configure_file(ClassName.hpp
  ${CMAKE_BINARY_DIR}/include/ClassName.hpp
)

### Add source file for the class ClassName.
file(GLOB SOURCES ClassName.cpp)

### Add library for the class ClassName.
add_library(your_lib_name ${SOURCES})



################################################################################
#    Folder that contains the class ClassNameA, that needs ClassNameB.         #
################################################################################

### Add header file for the class ClassNameA.
configure_file(ClassNameA.hpp
  ${CMAKE_BINARY_DIR}/include/ClassNameA.hpp
)

### Add source file for the class ClassNameA.
file(GLOB SOURCES ClassNameA.cpp)

### Add library for the class ClassNameA.
add_library(your_lib_name_a ${SOURCES})

### Link library for class ClassNameB to class ClassNameA.
target_link_libraries(your_lib_name_a your_lib_name_b)



################################################################################
#        Folder that contains the file with the recurrent includes             #
################################################################################

# Add the header file.
configure_file(G4ProjectInclude.hpp
  ${CMAKE_BINARY_DIR}/include/G4ProjectInclude.hpp
)
