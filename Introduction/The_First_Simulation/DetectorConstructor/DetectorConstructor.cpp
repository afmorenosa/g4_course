// Class header.
#include "DetectorConstructor.hpp"

/**
* Constructor of the DetectorConstructor class.
* Do nothing, just call the constructor of G4VUserDetectorConstruction class.
*/
DetectorConstructor::DetectorConstructor () : G4VUserDetectorConstruction() {}

/**
* Destructor of the DetectorConstructor class.
* Do nothing.
*/
DetectorConstructor::~DetectorConstructor () {}

/**
* Construct the detector.
* It will search air material in the NIST database, then create a box made by
* air an place it at (0,0,0) to be the world.
*/
G4VPhysicalVolume *DetectorConstructor::Construct () {
  // Manager for NIST db, for material searching.
  G4NistManager *nist = G4NistManager::Instance();

  // Set the material of the world as air.
  G4Material *world_mat = nist->FindOrBuildMaterial("G4_AIR");

  // Build world geometry with dimensions 2x, 2y and 2z.
  G4Box *world_box = new G4Box("World Box", .5*m, .5*m, .5*m);

  // Build logical volumes.
  G4LogicalVolume *world_log = new G4LogicalVolume(
    world_box,
    world_mat,
    "World"
  );

  // Place the world volume.
  G4VPhysicalVolume *world = new G4PVPlacement(
    0,
    G4ThreeVector(0, 0, 0),
    world_log,
    "Physical World",
    0,
    false,
    0,
    true
  );

  return world;
}

//
// DetectorConstructor.cpp end here
//
