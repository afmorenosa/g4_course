#ifndef DETECTOR_CONSTRUCTOR_HPP
#define DETECTOR_CONSTRUCTOR_HPP

// Geant4 Libs.
#include "G4VUserDetectorConstruction.hh"

// General includes.
#include "first_simulation.hpp"

/**
* This class will create the detector for the simulation.
* It will just create a world filled with air.
*/
class DetectorConstructor : public G4VUserDetectorConstruction {
public:
  // Class constructor.
  DetectorConstructor ();

  // Class destructor.
  virtual ~DetectorConstructor ();

  // Detector constructor function.
  virtual G4VPhysicalVolume *Construct();
};

#endif // DETECTOR_CONSTRUCTOR_HPP
