#ifndef FIRST_SIMULATION_HPP
#define FIRST_SIMULATION_HPP

// System Of Units
#include "G4SystemOfUnits.hh"

// Elements and materials for the volumes
#include "G4NistManager.hh"
#include "G4Material.hh"

// Geometry
#include "G4Box.hh"

// Volumes
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"

#endif // FIRST_SIMULATION_HPP
