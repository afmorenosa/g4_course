#ifndef PHYSICS_LIST_HPP
#define PHYSICS_LIST_HPP

// Geant4 Libs.
#include "G4VUserPhysicsList.hh"

// General includes.
#include "first_simulation.hpp"

/**
* This class will add the processes and the particles that are considered in jkhgsd kjshdg
* the simulation.
* It will just add the transportation process.
*/
class PhysicList : public G4VUserPhysicsList {
public:
  // Class constructor.
  PhysicList();

  // Class destructor.
  virtual ~PhysicList();

  // Add the processes for the simulation.
  virtual void ConstructProcess();
};

#endif // PHYSICS_LIST_HPP
