## Summary

(Explain the error)

## Correction proposal

(Describe the information that is incorrect, why and which should be the
correct one)

## Page or screenshot

(Please give the url of the page with the typo or a screenshot of the error.)

## Acknowledgment

(I really your appreciate the help, so that, I want to create a special part
for acknowledgment, in the page as well as in the repo readme, with the names
of the users who participate in this issues, if you want to be there write yes,
otherwise write no. Thanks for your help!)
